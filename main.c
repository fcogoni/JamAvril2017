/**
 * projet : Welcome to Cholestan, adaptation pour windows
 * auteur projet : Floriane Cogo Elouan
 * @author template Alexandre "LittleWhite" Laurent
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include "fct_jeu.h"
#include "menu.h"







void cleanup()
{
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}

int main(int argc, char* argv[])
{
    /** variable SDL */
    SDL_Window* window = NULL; /* Ecran principal */
    int quit = 0; /* Indicateur boolean pour la boucle principale */
    int imgFlags = IMG_INIT_JPG|IMG_INIT_PNG|IMG_INIT_TIF; /* Flags pour charger le support du JPG, PNG et TIF *

    /** variable autre */

    int fonction = 2;

    /** Demarre SDL et initialise valeur */
    if ( SDL_Init(SDL_INIT_VIDEO) == -1 )
    {
        fprintf(stderr,"Erreur lors de l'initialisation de la SDL\n");
        return -1;
    }

    if ( IMG_Init(imgFlags) != imgFlags )
    {
        fprintf(stderr,"Erreur lors de l'initialisation de la SDL_image : '%s'\n",IMG_GetError());
        cleanup();
        return -1;
    }

    if ( TTF_Init() == -1 )
    {
        fprintf(stderr,"Erreur lors de l'initialisation de la SDL_TTF : '%s'\n",TTF_GetError());
        cleanup();
        return -1;
    }

    srand(time(0));

    /** Création de la fenêtre */
    window = SDL_CreateWindow("Welcome to Cholestan",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,700,700,SDL_WINDOW_SHOWN);

    if ( window != NULL )
    {
        while (!quit)
        {
            switch(fonction)
            {
                case 0 :
                    ecrandemarage(window,&fonction,&quit);
                    break;
                case 1 :
                    ecransuppr(window,&fonction,&quit);
                    break;
                case 2 :
                    ecranjeu(window,&fonction,&quit);
                    break;
                case 3 :
                    ecranfin(window,&fonction,&quit);
                    break;
                default:
                    quit = 1;

            }
        }
        SDL_DestroyWindow(window);
        cleanup();
    }
    else
    {
        printf("Erreur de création de la fenêtre: %s\n",SDL_GetError());
        cleanup();
        return -3;
    }
}
